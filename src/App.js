import React, { useState, useEffect } from "react";
import QrReader from "react-qr-reader";
import { AiOutlineQrcode } from "react-icons/ai";

import { format } from "date-fns";
import pt from "date-fns/locale/pt";

import "./App.css";

import api from "./services/api";

function App() {
  const [result, setResult] = useState("");
  const [scanned, setScanned] = useState(false);
  const [clock, setClock] = useState();

  const handleScan = async (data) => {
    console.log(data);

    if (data) {
      setScanned(true);

      const response = await api.post(
        "SGHC-HospitalDeCamapanha/presenca.jsp&codigo=1"
      );
    }
  };

  const handleError = (err) => {
    console.error(err);
  };

  const now = new Date();

  const dateFormatted = format(now, "iiii, dd 'de' MMMM 'de' yyyy", {
    addSuffix: true,
    locale: pt,
  });

  useEffect(() => {
    const timer = setTimeout(() => {
      setClock(format(now, "hh:mm aa"));
      console.log("setClock");
    }, 1000);

    return () => {
      clearTimeout(timer);
    };
  }, [now]);

  return (
    <div className="container">
      <header>
        <h2>Hospital Geral do Estado de Alagoas</h2>
      </header>

      <div className="current-date">
        <p className="clock">{clock}</p>
        <p className="date">
          <i>{dateFormatted}</i>
        </p>
      </div>

      <div className="reader-container">
        <div className="reader-qr">
          <QrReader
            style={{ width: "100%" }}
            onError={handleError}
            onScan={scanned ? () => {} : handleScan}
          />
        </div>

        <div className="reader-info">
          <AiOutlineQrcode size={200} color="#000" />
          <p>Apresente sua identificação</p>
          <p>Obrigatório a validação do crachá</p>
        </div>
      </div>
    </div>
  );
}

export default App;
